# new sett GUI with Tauri

Live version: <https://biomedit.gitlab.io/presentations/rdm>

## Run locally

```bash
git submodule update --init
ln -s reveal.js/{dist,plugin} .
```

Then open `index.html` in your browser.

## Useful Links

- [Reveal.js](https://revealjs.com/)
